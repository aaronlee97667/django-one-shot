from django.urls import path
from todos.views import view_todolist, view_detail, create_view, update_view, delete_view, task_view, task_update


urlpatterns = [
    path("", view_todolist, name="todo_list_list"),
    path("<int:id>/", view_detail, name="todo_list_detail"),
    path("create/", create_view, name="todo_list_create"),
    path("<int:id>/edit/", update_view, name="todo_list_update"),
    path("<int:id>/delete/", delete_view, name="todo_list_delete"),
    path("items/create/", task_view, name="todo_item_create"),
    path("items/<int:id>/edit/", task_update, name="todo_item_update"),
]
