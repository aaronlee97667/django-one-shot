from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def view_todolist(request):
    context = {"todo_list": TodoList.objects.all()}
    return render(request, "todos/todo.html", context)


def view_detail(request, id):
    todo_with_task = get_object_or_404(TodoList, id=id)
    context = {"todo_with_task": todo_with_task}
    return render(request, "todos/tododetails.html", context)


def create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_view(request, id):
    todolist_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todolist_instance.id)
    else:
        form = TodoListForm(instance=todolist_instance)
    context = {"todolist_instance": todolist_instance, "form": form}
    return render(request, "todos/update.html", context)


def delete_view(request, id):
    delete_instance = TodoList.objects.get(id=id)
    print("request: ", request)
    print("id: ", id)
    if request.method == "POST":
        delete_instance.delete()
        return redirect("todo_list_list")
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/delete.html", context)


def task_view(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        print("Request: ", request)
        print("Id: ", id)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_task.html", context)


def task_update(request, id):
    tasklist_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=tasklist_instance)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=tasklist_instance)
    context = {"form": form}
    return render(request, "todos/update_task.html", context)


